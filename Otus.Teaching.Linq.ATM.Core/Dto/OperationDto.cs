﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Collections.Generic;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class OperationDto
    {
        public IEnumerable<OperationsHistory> Histories { get; set; }
        public User User { get; set; }
    }
}