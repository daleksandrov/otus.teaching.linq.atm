﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Dto
{
    public class AccountDto
    {
        public Account Account { get; set; }
        public IEnumerable<OperationsHistory> Histories { get; set; }
    }
}
