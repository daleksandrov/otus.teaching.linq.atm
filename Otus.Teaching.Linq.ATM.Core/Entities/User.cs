﻿using System;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string MiddleName { get; set; }
        public string Phone { get; set; }
        public string PassportSeriesAndNumber { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        
        public override string ToString()
        {
            return string.Format("Id: {0}, FirstName: {1}, SurName: {2}, MiddleName: {3}, Phone: {4}, PassportSeriesAndNumber: {5}, RegistrationDate: {6}, Login: {7}", Id, FirstName, SurName, MiddleName, Phone, PassportSeriesAndNumber, RegistrationDate, Login);
        }
    }
}