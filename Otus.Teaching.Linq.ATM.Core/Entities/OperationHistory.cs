﻿using System;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class OperationsHistory
    {
        public int Id { get; set; }
        public DateTime OperationDate { get; set; }
        public OperationType OperationType { get; set; }
        public decimal CashSum { get; set; }
        public int AccountId { get; set; }
        public override string ToString()
        {
            return string.Format("Id: {0}, OperationDate: {1}, OperationType: {2}, CashSum: {3}", Id, OperationDate, OperationType.ToString(), CashSum);
        }
    }
}