﻿using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Dto;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата

        /// <summary>
        /// Вывод информации о заданном аккаунте по логину и паролю;
        /// </summary>
        /// <returns></returns>
        public User GetUserByLoginAndPassword(string login, string password)
        {
            var query = from u in Users
                        where u.Login.Equals(login) && u.Password.Equals(password)
                        select u;

            return query.FirstOrDefault();
        }

        /// <summary>
        /// Вывод данных о всех счетах заданного пользователя;
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IEnumerable<Account> GetAccountsByUser(User user)
        {
            var query = from a in Accounts
                        where a.UserId == user.Id
                        select a;

            return query;
        }

        /// <summary>
        /// Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IEnumerable<AccountDto> GetAccountsWithHistoryByUser(User user)
        {
            var query = from a in Accounts
                        where a.UserId == user.Id
                        select new AccountDto()
                        { 
                            Account = a,
                            Histories = (from h in History where h.AccountId == a.Id select h)
                        };

            return query;
        }

        /// <summary>
        /// Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта;
        /// </summary>
        public IEnumerable<OperationDto> GetInfoAccountsWithInputCashOperation()
        {
            var query = from a in Accounts
                        join u in Users on a.UserId equals u.Id
                        select new OperationDto()
                        {
                            User = u,
                            Histories = (from h in History 
                                         where h.AccountId == a.Id && h.OperationType == OperationType.InputCash 
                                         select h)
                        };

            return query.Where(x=>x.Histories != null);
        }

        /// <summary>
        /// Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
        /// </summary>
        public IEnumerable<string> GetUsersByAccountSum(decimal N)
        {
            var query = from u in Users
                        join acc in Accounts on u.Id equals acc.UserId
                        where acc.CashAll > N
                        select u.SurName + " " + u.MiddleName + $" (Счет Id{acc.Id}): "  + acc.CashAll;

            return query;
             
        }
    }
}