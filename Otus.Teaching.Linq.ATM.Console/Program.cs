﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            //TODO: Далее выводим результаты разработанных LINQ запросов


            System.Console.WriteLine("1 Вывод информации о заданном аккаунте по логину и паролю");
            var user = atmManager.GetUserByLoginAndPassword("snow", "111");
            System.Console.WriteLine(user.ToString());

            System.Console.WriteLine(System.Environment.NewLine);
            System.Console.WriteLine("2 Вывод данных о всех счетах заданного пользователя");
            var accounts = atmManager.GetAccountsByUser(user);
            foreach(var acc in accounts)
            {
                System.Console.WriteLine(acc.ToString());
            }

            System.Console.WriteLine(System.Environment.NewLine);
            System.Console.WriteLine("3 Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту");
            var accountHistories = atmManager.GetAccountsWithHistoryByUser(user);
            foreach(var accountDto in accountHistories)
            {
                System.Console.WriteLine(accountDto.Account.ToString());
                
                foreach(var history in accountDto.Histories)
                {
                    System.Console.WriteLine(history.ToString());
                }

                System.Console.WriteLine(System.Environment.NewLine);
            }

            System.Console.WriteLine(System.Environment.NewLine);
            System.Console.WriteLine("4 Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта");
            var operationsInputCash = atmManager.GetInfoAccountsWithInputCashOperation();
            foreach(var op in operationsInputCash)
            {
                System.Console.WriteLine(op.User.SurName + " " + op.User.MiddleName);

                foreach (var history in op.Histories)
                {
                    System.Console.WriteLine(history.ToString());
                }

                System.Console.WriteLine(System.Environment.NewLine);
            }

            System.Console.WriteLine(System.Environment.NewLine);
            System.Console.WriteLine("5 Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)");
            decimal N = 1500m;
            var userCashs = atmManager.GetUsersByAccountSum(N);
            foreach(var uc in userCashs)
            {
                System.Console.WriteLine(uc);
            }

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

       
        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}